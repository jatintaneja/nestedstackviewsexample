//
//  ViewController.swift
//  NestedStackViewsExample
//
//  Created by Jatin Taneja on 5/8/19.
//  Copyright © 2019 Dizzup Inc. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var textView: UITextView!
    
    @IBOutlet var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        scrollView.contentInset.bottom=400 //adding a bit of offset to the scroll view ensures that the textfield will move up automatically, when the keyboard hides it
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardFrameChanges),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //textField.resignFirstResponder()
        return true
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event) //always call the super first
        //textView.resignFirstResponder() // this is one way to do it
        //view.endEditing(true) //this is another way to dismiss the keyboard
    }
    
    @objc func keyboardFrameChanges(notification:Notification) {
        
    }


}

